# install dependencies

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y default-jdk-headless git python3 python3-pip gradle make

RUN pip3 install mock pylint pylint-quotes pytest pytest-cov awscli cfn-lint virtualenv pipenv 'setuptools>=41'



# import keys

RUN mkdir /root/.ssh

COPY .jenkins/known_hosts /root/.ssh/known_hosts



USER root



# Warm up Gradle

RUN mkdir /root/gradleWarmUp

COPY .jenkins/build.gradle /root/gradleWarmUp/build.gradle

WORKDIR /root/gradleWarmUp

# 'com.palantir.git-version' plugin requires .git directory

RUN mkdir .git

RUN gradle warmUp



WORKDIR /app